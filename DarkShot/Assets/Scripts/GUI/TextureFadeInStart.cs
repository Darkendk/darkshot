﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextureFadeInStart : MonoBehaviour
{
    public float fadespeed = 1;
    bool startfade = false;
    float alpha = 1;
    Image img;

    // Use this for initialization
    void Awake()
    {
        img = gameObject.GetComponent<Image>();
        img.color = new Color(255, 255, 255, 0);
    }

    void Start()
    {
        MenuCameraChoose.Instance.GameStart += StartFade;
        startfade = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (startfade == true)
        {

            float alphanow = img.color.a;
            if (alphanow < 1)
            {
                alphanow += Time.unscaledDeltaTime * fadespeed;
                img.color = new Color(255, 255, 255, alphanow);
            }
        }
    }
    void StartFade()
    {
       
    }
}
