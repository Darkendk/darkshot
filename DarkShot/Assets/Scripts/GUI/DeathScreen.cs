﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DeathScreen : MonoBehaviour
{
    public Image vrDeathScreen1;
    public Image vrDeathScreen2;
    public Image deathScreen;
    public float fadespeed = 1;
    bool fadein = false;
    public event Action FadeDone;
    // Use this for initialization
    void Start()
    {
        Player.Instance.PlayerDead += FadeinDeath;
    }

    // Update is called once per frame
    void Update()
    {
        if (fadein == true)
        {
            if (MenuCameraChoose.Instance.platform == MenuCameraChoose.PlatformPlayingon.VR)
            {
                fadeinimg(vrDeathScreen1);
                fadeinimg(vrDeathScreen2);
            }
            else
            {
                fadeinimg(deathScreen);
            }
        }
    }
    void FadeinDeath()
    {
        vrDeathScreen1.color = new Color(255, 255, 255, 0);
        vrDeathScreen2.color = new Color(255, 255, 255, 0);
        deathScreen.color = new Color(255, 255, 255, 0);
        fadein = true;
        Time.timeScale = 0;
        Player.Instance.PlayerDead -= FadeinDeath;
    }
    void fadeinimg(Image img)
    {
        float alphanow = img.color.a;
        if (alphanow < 1)
        {
            alphanow += Time.unscaledDeltaTime * fadespeed;
            img.color = new Color(255, 255, 255, alphanow);
        }
        else
        {
            if(FadeDone != null)
            { FadeDone(); }
        }
    }

}
