﻿using UnityEngine;
using System.Collections;

public class GlobalGameStart : MonoBehaviour
{
    public enum PlatformToPlay { VR, Mobile, PC}
    public PlatformToPlay platform;
    bool gamestarted = false;
    bool inputenabled = false;
    float timer = 0;
    public float startDelay = 1f;
    public GameObject mainMenuCamera;
    public GameObject mainMenu;
    public GameObject VRMenuCamera;
    public GameObject ShootVRScript;
    public GameObject VRMainMenu1;
    public GameObject VRMainMenu2;


    // Use this for initialization
    void Start()
    {
       
        if (platform == PlatformToPlay.VR)
        {
            CardboardMagnetSensor.Instance.init();
            VRMenuCamera.SetActive(true);
            VRMainMenu1.SetActive(true);
            VRMainMenu2.SetActive(true);
            mainMenuCamera.SetActive(false);
            mainMenu.SetActive(false);
        }
        else
        {
            VRMenuCamera.SetActive(false);
            VRMainMenu1.SetActive(false);
            VRMainMenu2.SetActive(false);
            mainMenuCamera.SetActive(true);
            mainMenu.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(inputenabled == false)
        {
            timer += Time.unscaledDeltaTime;
            if(timer > startDelay)
            {
                inputenabled = true;
            }
        }

        if (gamestarted == false && inputenabled == true)
        {

            if (platform == PlatformToPlay.VR)
            {
                CardboardMagnetSensor.Instance.magUpdate(Input.acceleration, Input.compass.rawVector);

                if (CardboardMagnetSensor.Instance.clicked() == true)
                {
                    MenuCameraChoose.Instance.PlayVR();
                    ShootVRScript.SetActive(true);
                    //VRMainMenu1.SetActive(false);
                    //VRMainMenu2.SetActive(false);
                    gamestarted = true;
                }

            }

            if (Input.GetButtonDown("Fire1"))
                {
                    if (platform == PlatformToPlay.Mobile)
                    {
                        MenuCameraChoose.Instance.PlayMobile();
                    }
                    if (platform == PlatformToPlay.VR)
                    {    
                        MenuCameraChoose.Instance.PlayVR();
                        ShootVRScript.SetActive(true);
                        //VRMainMenu1.SetActive(false);
                        //VRMainMenu2.SetActive(false);
                     }
                    if (platform == PlatformToPlay.PC)
                    {
                        MenuCameraChoose.Instance.PlayPC();
                    }
                        gamestarted = true;
            }
           
        }
    }
}
