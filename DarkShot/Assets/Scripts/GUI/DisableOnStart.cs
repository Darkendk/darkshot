﻿using UnityEngine;
using System.Collections;

public class DisableOnStart : MonoBehaviour
{
    public GameObject gameObjectToDisable;
    // Use this for initialization
    void Start()
    {
        MenuCameraChoose.Instance.GameStart += DisableAfterStart;
        if (gameObjectToDisable == null)
        {
            gameObjectToDisable = gameObject;
        }
    }

    // Update is called once per frame
void DisableAfterStart()
    {
        gameObjectToDisable.SetActive(false);
    }
}
