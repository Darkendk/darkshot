﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeoutTexture : MonoBehaviour
{
    public GameObject gameObjectToDisable;
    public float fadespeed = 2;
    bool startfade = false;
    float alpha = 1;
    Image img;
    
    // Use this for initialization
    void Start()
    {
        MenuCameraChoose.Instance.GameStart += StartFade;
        if (gameObjectToDisable == null)
        {
            gameObjectToDisable = gameObject;
        }
        img = gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(startfade == true)
        {
            
            float alphanow = img.color.a;
            if (alphanow > 0)
            {
                alphanow -= Time.unscaledDeltaTime * fadespeed;
                img.color = new Color(255, 255, 255, alphanow);
            }
            else
            {
                MenuCameraChoose.Instance.GameStart -= StartFade;
                gameObjectToDisable.SetActive(false);
            }
        }
    }
    void StartFade()
    {
        startfade = true;
    }
}
