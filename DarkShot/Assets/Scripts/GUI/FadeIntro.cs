﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeIntro : MonoBehaviour
{
    public float fadespeed = 1;
    bool startfade = false;
    float timer;
    float timeToShow = 3f;
    float alpha = 1;
    Image img;
    // Use this for initialization
    void Start()
    {
        img = gameObject.GetComponent<Image>();
        img.color = new Color(255, 255, 255, 0);
    }

    // Update is called once per frame
    void Update()
    {
        float alphanow = img.color.a;
        print(alphanow);
        if (startfade == false)
        {
            if (alphanow < 1)
            {
                alphanow += Time.deltaTime*fadespeed;
                img.color = new Color(255, 255, 255, alphanow);
            }
            else 
            {
                if (timer < timeToShow)
                {
                    timer += Time.deltaTime;
                }
                else
                {
                    startfade = true;
                }
            }
        }

        if (startfade == true)
        {
            if (alphanow > 0)
            {
                alphanow -= Time.deltaTime* fadespeed;
                img.color = new Color(255, 255, 255, alphanow);
            }
            else
            {
                Application.LoadLevel("Shipping");
            }
        }
    }
}
