﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextGraber : MonoBehaviour
{
    public Text text;
    // Use this for initialization
    void OnEnable()
    {
        //Invoke("getscore",1.5f);
        gameObject.GetComponent<Text>().text = text.text;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void getscore()
    {
        gameObject.GetComponent<Text>().text = text.text;
        Debug.Log("I " + this.gameObject.name + " Grabbed Text " + text.text);
    }
}
