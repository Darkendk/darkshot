﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootToFade : MonoBehaviour
{
    float alpha = 1;
    Image img;
    public float numbToDec = 0.2f;
    bool dontDec = false;
    // Use this for initialization
    void Awake()
    {
        img = gameObject.GetComponent<Image>();
        img.color = new Color(255, 255, 255, 1);
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(dontDec == false)
        { 
            if (Input.GetButtonDown("Fire1"))
            {
                    float alphanow = img.color.a;
                    alphanow -= numbToDec;
                    img.color = new Color(255, 255, 255, alphanow);
                if(alphanow < 0 )
                { dontDec = true; }
            }
        }
    }
}
