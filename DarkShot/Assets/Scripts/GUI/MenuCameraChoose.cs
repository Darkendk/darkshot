﻿using UnityEngine;
using System.Collections;
using System;

public class MenuCameraChoose : Singleton<MenuCameraChoose>
{
    public enum PlatformPlayingon {VR,Mobile,PC}
    [NonSerialized]
    public PlatformPlayingon platform;
    public event Action GameStart;
    #region Cameras
    public GameObject camera_VR;
    public GameObject camera_mobile;
    public GameObject camera_PC;
    public GameObject camera_Defult;
    #endregion

    #region UI
    public GameObject ui__PlayMenu;
    #endregion

   void Start()
    {
        Time.timeScale = 0;
    }
    public void PlayVR()
    {
        platform = PlatformPlayingon.VR;
        StartGame(camera_VR);
    }
    public void PlayMobile()
    {
        platform = PlatformPlayingon.Mobile;
        StartGame(camera_mobile);
    }
    public void PlayPC()
    {
        platform = PlatformPlayingon.PC;
        StartGame(camera_PC); 
    }
    private void StartGame(GameObject playingCamera)
    {
        GameStart();
        Time.timeScale = 1;
        playingCamera.SetActive(true);
        camera_Defult.SetActive(false);
       // ui__PlayMenu.SetActive(false);
    }
   public void BackToMenu()
    {
        ui__PlayMenu.SetActive(true);
        switch (platform)
        {
            case PlatformPlayingon.VR:
                camera_VR.SetActive(false);
                break;
            case PlatformPlayingon.Mobile:
                camera_mobile.SetActive(false);
                break;
            case PlatformPlayingon.PC:
                camera_PC.SetActive(false);
                break;
            default:
                break;
        }
        camera_Defult.SetActive(true);
        Time.timeScale = 0;
    }
}
