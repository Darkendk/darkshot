﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class SFXManager : MonoBehaviour
{
    const string shootEventPath = "event:/Shoot";
    const string bullethitEventPath = "event:/Bullet Impacts";


    //Fmod Events

    // Use this for initialization
    void Start()
    {
        Shoot.BulletShot += BulletShot;
        Bullet.OnBulletHit += BulletHit;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void BulletShot(Shoot shootInst)
    {
        RuntimeManager.PlayOneShot(shootEventPath, shootInst.gameObject.transform.position);
    }
    void BulletHit(Bullet BulletInst)
    {
        RuntimeManager.PlayOneShot(bullethitEventPath, BulletInst.gameObject.transform.position);
    }
    void OnDestroy()
    {
        Shoot.BulletShot -= BulletShot;
        Bullet.OnBulletHit -= BulletHit;
    }
}
