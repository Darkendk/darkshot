﻿using UnityEngine;
using System.Collections;

public class AudioMaster : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator FadeParameterTo(FMOD.Studio.EventInstance fmodEvent, string parameterName, float changeto, float parameterVal, float fadespeed, System.Action<float> valOut)
    {
       
        if (changeto > parameterVal)
        {
            while (parameterVal < changeto)
            {
                parameterVal += Time.deltaTime * fadespeed;
                fmodEvent.setParameterValue(parameterName, parameterVal);
                yield return new WaitForEndOfFrame();
            }
        }
        else if (changeto < parameterVal)
        {
            while (parameterVal > changeto)
            {
                parameterVal -= Time.deltaTime * fadespeed;
                fmodEvent.setParameterValue(parameterName, parameterVal);
                yield return new WaitForEndOfFrame();
            }
        }
        valOut(parameterVal);

    }
}
