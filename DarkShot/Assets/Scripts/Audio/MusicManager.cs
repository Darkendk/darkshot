﻿using UnityEngine;
using System.Collections;
using FMODUnity;

public class MusicManager : MonoBehaviour
{
    private FMOD.Studio.EventInstance musicevent;
    private FMOD.Studio.EventDescription eventDescription;
    private FMOD.Studio.ParameterInstance intensityParam;
    private FMOD.Studio.PARAMETER_DESCRIPTION intensityParamDes;
    private FMOD.Studio.PARAMETER_DESCRIPTION fadeParamDes;
    private FMOD.Studio.ParameterInstance deathParam;
    private FMOD.Studio.ParameterInstance meanuParam;
    private FMOD.Studio.ParameterInstance fadeParam;
    float musicfade = 0;
    float distanceToPlayer;
    public float fadeSpeed = 2f;
    public float fadeinSpeed = 2f;
    float musicParamerterMiltiplayer;
    // Use this for initialization
    void Start()
    {
        //Start Music
        eventDescription = RuntimeManager.GetEventDescription("event:/Music_Mix_2");
        eventDescription.createInstance(out musicevent);
        musicevent.start();
        // Get all the Paramerter of the Music event
        musicevent.getParameter("Intensity", out intensityParam);
        musicevent.getParameter("Death", out deathParam);
        musicevent.getParameter("Menu/Game", out meanuParam);
        musicevent.getParameter("MusicFade", out fadeParam);
        //Asign Action Events 
        Player.Instance.PlayerDead += PlayerDeath;
        MenuCameraChoose.Instance.GameStart += SartGameMusic;
        intensityParam.getDescription(out intensityParamDes); 
        fadeParam.getDescription(out fadeParamDes);
        musicParamerterMiltiplayer = intensityParamDes.maximum / SpawnManager.Instance.spawnDistance; // Gets the Max intencity correctly

    }

    // Update is called once per frame
    void Update()
    {
        if(SpawnManager.Instance.monsterList.Count != 0)
        {
            distanceToPlayer = Vector3.Distance(SpawnManager.Instance.monsterList[0].transform.position, Player.Instance.transform.position);
            float distanceparamValue = (SpawnManager.Instance.spawnDistance - distanceToPlayer) * musicParamerterMiltiplayer; 

            ChangePararVal(intensityParam, distanceparamValue, fadeSpeed);
        }
        if (musicfade < fadeParamDes.maximum)
        {
            musicfade += Time.unscaledDeltaTime* fadeinSpeed;
            ChangePararVal(fadeParam, musicfade, fadeinSpeed);
        }
        float value;
        fadeParam.getValue(out value);
    }
    void ChangePararVal(FMOD.Studio.ParameterInstance paramInst,float changeto,float fadespeed)
    {
        float parameterVal;
        paramInst.getValue(out parameterVal);
        if (changeto > parameterVal)
        {
            if (parameterVal < changeto)
            {
                parameterVal += Time.unscaledDeltaTime * fadespeed;
                paramInst.setValue(parameterVal);
            }
        }
        else if (changeto < parameterVal)
        {
            if (parameterVal > changeto)
            {
                parameterVal -= Time.unscaledDeltaTime * fadespeed;
                paramInst.setValue(parameterVal);
            }
        }
    }
    void PlayerDeath()
    {
        deathParam.setValue(1);
        Player.Instance.PlayerDead -= PlayerDeath;
        MenuCameraChoose.Instance.GameStart -= SartGameMusic;
    }
    void SartGameMusic()
    {
        meanuParam.setValue(1);
    }
    void OnDestroy()
    {     
             
    }
   
}
