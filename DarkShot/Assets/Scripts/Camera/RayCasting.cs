﻿using UnityEngine;
using System.Collections;

public class RayCasting : MonoBehaviour
{

    Camera mycamera;
    Light monsterlight;
    bool monsterlit;
    float originalLightInt;
    public float increceMonsLightBy =5;
    public bool chrosshair = false;
    public Texture2D crosshairImage;

    void Start()
    {
        mycamera = GetComponent<Camera>();
    }

    void Update()
    {
        Ray ray = mycamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        //if ( Physics.Raycast(transform.position, fwd, out hit, 100))
        if (Physics.Raycast(ray, out hit,100f))
        {
            if (hit.transform.gameObject.tag == "Monster")
            {
                if(monsterlit == false)
                { 
                    monsterlight = hit.transform.gameObject.GetComponentInChildren<Light>();
                    originalLightInt = monsterlight.intensity;
                    monsterlit = true;
                    monsterlight.intensity += increceMonsLightBy;
                }
            }
        }
        else if(monsterlit == true)
        {
            monsterlit = false;
            if(monsterlight!= null)
            {
                monsterlight.intensity = originalLightInt;
            }
        }
      
    }
    
}
