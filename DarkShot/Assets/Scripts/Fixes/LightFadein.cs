﻿using UnityEngine;
using System.Collections;

public class LightFadein : MonoBehaviour
{
    Light mylight;
    public float Speed = 2;
    public float maxIntLight = 7;
    // Use this for initialization
    void Start()
    {
        mylight = gameObject.GetComponent<Light>();
        mylight.intensity = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (mylight.intensity < maxIntLight)
        {
            mylight.intensity += Time.deltaTime * Speed;
        }
    }
}
