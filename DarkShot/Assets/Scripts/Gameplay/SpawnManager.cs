﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpawnManager : Singleton<SpawnManager>
{
    public GameObject[] spawnpoints;
    float spawntimer;
    float phasetimer;
    float phase1Uptimers;
    float numberOfPhases1Passed = 0;
    float timeToRemoveEachJump;
    float phase = 1;
    [HideInInspector]
    public int numberOfMonsters;
    public int maxMonstersToHear = 4;
    public float spawnDistance = 65f;
    public List<GameObject> monsterList = new List<GameObject>();
    // Open Variables 
    public float timeBetweenSpawns = 5;
    public float maxRandomSecBetweenSapwns = 10;
    public float minRandomSecBetweenSapwns = 2;
    public float phase1Timer = 120;
    public float phase1Divitions = 4;
    public float phase2Timer = 30;
    public float deathRoundSpawnTime = 0.5f;

    public event Action OnMonsterDead;
    Animator monsterAnim;
    void SetRandomSpawntime()
    {
        if (phase == 1) // checks what phase it is 
        {
            if (phasetimer > phase1Uptimers) // checks if it is time to ramp up the Spawn rate by the intervals in Phase 1 
            {
                    if (numberOfPhases1Passed == (phase1Divitions - 1))    // Checks if it is time to Phase 2
                    {
                        maxRandomSecBetweenSapwns = minRandomSecBetweenSapwns; // Sets Max and min to the same value
                        phase = 2;
                    }
                    else
                    {
                        maxRandomSecBetweenSapwns -= timeToRemoveEachJump; // Removes from the Max a set a value that is calculated on Start
                    }
                numberOfPhases1Passed++;
                phasetimer = 0;
            }
                timeBetweenSpawns = UnityEngine.Random.Range(minRandomSecBetweenSapwns, maxRandomSecBetweenSapwns);
        }
        else if (phase == 2)
        {
            if (phasetimer > phase2Timer)
            {
                timeBetweenSpawns = deathRoundSpawnTime;
            }
        }

        spawntimer = 0;
    }

    // Use this for initialization
    void Start()
    {
        foreach (GameObject spwnpnt in spawnpoints)
        {
            spwnpnt.transform.localPosition = new Vector3(0, Player.Instance.transform.position.y, spawnDistance);
        }
        phase1Uptimers = phase1Timer / phase1Divitions; //Sets the time between ceach diffucly jump during phase 1
        timeToRemoveEachJump = (maxRandomSecBetweenSapwns - minRandomSecBetweenSapwns) / phase1Divitions; // sets the amout if time to remove from the Max value of the Random in each Difficulty jump
        MenuCameraChoose.Instance.GameStart += RestartSpawners;
    }

    // Update is called once per frame
    void Update()
    {
        phasetimer += Time.deltaTime;
        spawntimer += Time.deltaTime;
        if (spawntimer > timeBetweenSpawns)
        {
            SpawnMonster();
            SetRandomSpawntime();
        }
    }
    void SpawnMonster()
    {
        int randomNumber = UnityEngine.Random.Range(0, spawnpoints.Length);
        GameObject monster = Instantiate(Resources.Load("Monster", typeof(GameObject)), spawnpoints[randomNumber].transform.position,Quaternion.identity) as GameObject;
        monsterList.Add(monster);
        monster.GetComponent<Monster>().OnMonsterDied += KillMonster;
        numberOfMonsters++;
        if(numberOfPhases1Passed > 2)
        {
            monster.GetComponent<Monster>().moveSpeed = monster.GetComponent<Monster>().moveSpeed * 1.5f;
            monsterAnim = monster.GetComponentInChildren<Animator>();
            monsterAnim.speed = 1.3f;
        }
        if(phase == 2)
        {
            monster.GetComponent<Monster>().moveSpeed = monster.GetComponent<Monster>().moveSpeed * 2f;
            monsterAnim = monster.GetComponentInChildren<Animator>();
            monsterAnim.speed = 1.8f;
        }
    }
    void KillMonster(Monster monster)
    {
        monsterList.Remove(monster.gameObject);
        numberOfMonsters--;
        monster.OnMonsterDied -= KillMonster;
        Destroy(monster.gameObject);
        TurnOnMonsters();

        if (OnMonsterDead != null)
        {
            OnMonsterDead();
        }
    }
   void RestartSpawners()
    {
        //Reset Spawners
    }
    void TurnOnMonsters()
    {
        for (int i = 0; i < numberOfMonsters ; i++)
        {
            if( i < maxMonstersToHear)
            { 
                Monster mymonster = monsterList[i].GetComponent<Monster>();
                if(mymonster.playingSound == false)
                {
                    mymonster.PlayMonsterSound();
                }
            }
        }
    }
    void OnDestroy()
    {
        if (MenuCameraChoose.Instance != null)
        { 
            MenuCameraChoose.Instance.GameStart -= RestartSpawners;
        }

        for (int i = 0; i < numberOfMonsters; i++)
        {
            if(monsterList[i] != null)
            { 
            Monster mymonster = monsterList[i].GetComponent<Monster>();
            mymonster.OnMonsterDied -= KillMonster;
            }
        }
    }


}
