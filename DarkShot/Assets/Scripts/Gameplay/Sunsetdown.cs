﻿using UnityEngine;
using System.Collections;

public class Sunsetdown : MonoBehaviour
{
    public float sunSetSpeed;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.left * (sunSetSpeed * Time.deltaTime));
    }
}
