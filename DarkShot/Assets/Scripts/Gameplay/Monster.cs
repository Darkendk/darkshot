﻿using UnityEngine;
using System.Collections;
using System;
using FMODUnity;

public class Monster : MonoBehaviour
{
    public float moveSpeed = 2f;
    public event Action<Monster> OnMonsterDied;

    const string creatureEventPath = "event:/Creatures";
    private FMOD.Studio.EventInstance creaturesEvent;
    private FMOD.Studio.EventDescription creaturesEventDescription;
    private FMOD.Studio.ParameterInstance creatureFadeParam;
    bool fadeLastMonsyer = false;

    StudioEventEmitter emmiter;
    public bool playingSound = false;
    public GameObject deathParticle;
    Animator monsterAnim;

    public bool stopSoundGameOver = true;
    // Use this for initialization
    void Start()
    {
        creaturesEventDescription = RuntimeManager.GetEventDescription(creatureEventPath);
        creaturesEventDescription.createInstance(out creaturesEvent);
        emmiter = gameObject.GetComponent<StudioEventEmitter>();
        creaturesEvent.getParameter("Fade", out creatureFadeParam);

        if (SpawnManager.Instance.numberOfMonsters <= SpawnManager.Instance.maxMonstersToHear)
        {
            PlayMonsterSound();
        }
        Player.Instance.PlayerDead += Stopsound;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Player.Instance.transform.position, Time.deltaTime * moveSpeed);
        gameObject.transform.LookAt(Player.Instance.gameObject.transform);
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            moveSpeed = moveSpeed * 2;
            monsterAnim = gameObject.GetComponentInChildren<Animator>();
            monsterAnim.speed = 2;
        }

        if (fadeLastMonsyer == true)
        {
            print("Fadeout in Update");
            ChangePararVal(creatureFadeParam, 1, 1);
        }
    }
    
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Bullet")
        {
            Player.Instance.PlayerDead -= Stopsound;

            if (OnMonsterDied != null)
                OnMonsterDied(this);

            if (deathParticle != null)
            {
                Instantiate(deathParticle, gameObject.transform.position, Quaternion.Euler(-45, 0, 0));
            }
           

        }
    }
    void OnDestroy()
    {
        emmiter.Stop();
    }
    public void PlayMonsterSound()
    {
       
        //emmiter.enabled = true;
        emmiter.Play();
        playingSound = true;
        //creaturesEvent.start();
        //bool is3d = false;
        //creaturesEventDescription.is3D(out is3d);
        //if (is3d)
        //{
        //    enabled = true;
        //}
    }
   public void Stopsound()
    {
        
        if (stopSoundGameOver == true)
        {
            emmiter.Stop();
        }
        else if (stopSoundGameOver == false)
        {
            //Fade out the last one 
            print("Fadeout Start");
            emmiter.Stop();
            //creaturesEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            //fadeLastMonsyer = true;
        }
    }
    void ChangePararVal(FMOD.Studio.ParameterInstance paramInst, float changeto, float fadespeed)
    {
        float parameterVal;
        paramInst.getValue(out parameterVal);
        if (changeto > parameterVal)
        {
            if (parameterVal < changeto)
            {
                parameterVal += Time.unscaledDeltaTime * fadespeed;
                paramInst.setValue(parameterVal);
            }
        }
        else if (changeto < parameterVal)
        {
            if (parameterVal > changeto)
            {
                parameterVal -= Time.unscaledDeltaTime * fadespeed;
                paramInst.setValue(parameterVal);
            }
        }
        paramInst.getValue(out parameterVal);
        print("Fade val now :" + parameterVal);
    }
}
