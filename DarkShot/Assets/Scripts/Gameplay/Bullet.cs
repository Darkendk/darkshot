﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public float speed =1f;
    public float bulletLifeSpan = 10f;
    Rigidbody myrigidbody;
    public static event Action<Bullet> OnBulletHit;
    // Use this for initialization
    void Start()
    {
        myrigidbody = gameObject.GetComponent<Rigidbody>();
        myrigidbody.velocity = transform.TransformDirection(Vector3.forward*speed);
        Destroy(gameObject, bulletLifeSpan);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Monster")
        {
            if (OnBulletHit != null)
                OnBulletHit(this);
            Destroy(gameObject,0.1f);
        }
    }
   
}
