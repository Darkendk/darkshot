﻿using UnityEngine;
using System.Collections;
using System;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public static event Action<Shoot> BulletShot;
    public static event Action RestartGame;
    bool playerIsDead = false;
    // Use this for initialization
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Player.Instance.PlayerDead += PlayerDead;
        CardboardMagnetSensor.Instance.init();
    }

    // Update is called once per frame
    void Update()
    {

            if (MenuCameraChoose.Instance.platform == MenuCameraChoose.PlatformPlayingon.VR)
            {
                CardboardMagnetSensor.Instance.magUpdate(Input.acceleration, Input.compass.rawVector);

                if (CardboardMagnetSensor.Instance.clicked() == true)
                {
                    if (playerIsDead == false)
                    {
                        Instantiate(bullet, gameObject.transform.position, gameObject.transform.rotation);
                        //CardboardMagnetSensor.ResetClick();
                        if (BulletShot != null)
                        { BulletShot(this); }
                    }
                    else
                    {
                        if (RestartGame != null)
                        { RestartGame(); }
                    }
                }

            }
                if (Input.GetButtonDown("Fire1"))
                {
                    if (playerIsDead == false)
                    {
                        Instantiate(bullet, gameObject.transform.position, gameObject.transform.rotation);
                        if (BulletShot != null)
                        { BulletShot(this); }
                    }
                    else
                    {
                        if (RestartGame != null)
                        { RestartGame(); }
                    }
                }
            
    }
    void PlayerDead()
    {
        playerIsDead = true;
        Player.Instance.PlayerDead -= PlayerDead;
    }

}
