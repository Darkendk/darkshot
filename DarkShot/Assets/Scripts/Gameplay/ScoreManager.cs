﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public GameObject scoreUI;
    public GameObject VRscoreUI1;
    public GameObject VRscoreUI2;
    public Text scoreNumber;
    public Text bestNumber;
    public Text menuBestScore;
    public Text vRBestScore1;
    public Text vRBestScore2;
    float best = 0;
    float score = 0;
    public float scoreMultiplayer = 10;
    public float monsterScoreBosst = 200;
    public DeathScreen deathscreen;

    // Use this for initialization
    void Start()
    {
        SpawnManager.Instance.OnMonsterDead += MonsterScoreUp;
        deathscreen = gameObject.GetComponent<DeathScreen>();
        deathscreen.FadeDone += GameOver;
        Shoot.RestartGame += RestartGame;
        best = PlayerPrefs.GetFloat("BestScore", 0); 
        menuBestScore.text = ("High Score: " + best.ToString("#####"));
        vRBestScore1.text = ("High Score: " + best.ToString("#####"));
        vRBestScore2.text = ("High Score: " + best.ToString("#####"));
        bestNumber.text = best.ToString("#####");
    }

    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime * scoreMultiplayer;
    }
    void MonsterScoreUp()
    {
        score += monsterScoreBosst;
    }

    void GameOver()
    {
        PresentScore();
        if (MenuCameraChoose.Instance.platform == MenuCameraChoose.PlatformPlayingon.VR)
        {
            VRscoreUI1.SetActive(true);
            VRscoreUI2.SetActive(true);
        }
        else
        {
            scoreUI.SetActive(true);
        }
        
       // InvokeRealTime("RestartGame", 5);

        if (SpawnManager.Instance != null)
        {
            SpawnManager.Instance.OnMonsterDead -= MonsterScoreUp;
        }

        if (Player.Instance != null)
        {
            Player.Instance.PlayerDead -= GameOver;
        }
    }

    void PresentScore()
    {
        scoreNumber.text = score.ToString("#####");
        if( score >= best)
        {
            best = score;
            PlayerPrefs.SetFloat("BestScore", score);
            bestNumber.text = best.ToString("#####");
            bestNumber.color = Color.green;
            scoreNumber.color = Color.green;
        }
    }

     void OnDestroy()
    {
     
    }



    void RestartGame()
    {
        Shoot.RestartGame -= RestartGame;
        deathscreen.FadeDone -= GameOver;
        SpawnManager.Instance.OnMonsterDead -= MonsterScoreUp;
        UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene.name);
    }

    public void InvokeRealTime(string functionName, float delay)
    {
        StartCoroutine(InvokeRealTimeHelper(functionName, delay));
    }
    private IEnumerator InvokeRealTimeHelper(string functionName, float delay)
    {
        float timeElapsed = 0f;
        while (timeElapsed < delay)
        {
            timeElapsed += Time.unscaledDeltaTime;
            yield return null;
        }
        SendMessage(functionName);
    }
}
