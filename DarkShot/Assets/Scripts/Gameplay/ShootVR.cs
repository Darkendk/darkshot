﻿using UnityEngine;
using System.Collections;


//public static XmlDocument XmlDoc;
//public static XmlNodeList xnl;
//public TextAsset TA;

public class ShootVR : MonoBehaviour
{
    public GameObject bullet;
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        CardboardMagnetSensor.Instance.init();
    }
    void Update()
    {
            CardboardMagnetSensor.Instance.magUpdate(Input.acceleration,Input.compass.rawVector);

            if (CardboardMagnetSensor.Instance.clicked() == true)
            {
                Instantiate(bullet, gameObject.transform.position, gameObject.transform.rotation);
                //CardboardMagnetSensor.ResetClick();
            }

    }
}