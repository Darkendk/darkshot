﻿using UnityEngine;
using System.Collections;
using System;

public class Player : Singleton<Player>
{
    public event Action PlayerDead;
    public bool playerisdead = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Monster")
        {
            Monster monsterscript = col.gameObject.GetComponent<Monster>();
            monsterscript.stopSoundGameOver = false;
            if (PlayerDead != null)
            PlayerDead();
            playerisdead = true;    
        }
    }
  
}
